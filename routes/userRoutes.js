const { Router } = require("express");
const UserService = require("../services/userService");
const {
  createUserValid,
  updateUserValid,
} = require("../middlewares/user.validation.middleware");
const { responseMiddleware } = require("../middlewares/response.middleware");

const router = Router();

// TODO: Implement route controllers for user
router.get(
  "/",
  async (req, res, next) => {
    try {
      const users = await UserService.fetchAll();
      res.data = users;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.get(
  "/:id",
  async function (req, res, next) {
    try {
      const user = await UserService.search({ id: req.params.id });
      if (!user) {
        res.status(404);
        throw new Error("User not found");
      }
      res.data = user;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.post(
  "/",
  createUserValid,
  async (req, res, next) => {
    try {
      const check = await UserService.checkExistance({
        email: req.body.email,
        phoneNumber: req.body.phoneNumber,
      });

      if (check) {
        res.status(400);
        throw new Error("User already exists!");
      }

      const user = await UserService.create(req.body);
      res.data = user;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.delete(
  "/:id",
  async (req, res, next) => {
    try {
      if (!UserService.checkExistance({ id: req.params.id })) {
        res.status(404);
        throw new Error("User doesn`t exist!");
      }
      const user = await UserService.delete(req.params.id);
      res.data = user;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.put(
  "/:id",
  updateUserValid,
  async (req, res, next) => {
    try {
      if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400);
        throw new Error("Update body must be not null!");
      }
      if (req.body.email) {
        req.body.email = req.body.email.toLowerCase();
      }
      if (!UserService.checkExistance({ id: req.params.id })) {
        res.status(404);
        throw new Error("User doesn`t exist!");
      }

      const checkExistance = await UserService.checkExistance({
        email: req.body.email,
        phoneNumber: req.body.phoneNumber,
      });
      if (checkExistance) {
        res.status(400);
        throw new Error("User with such email or phone already exists!");
      }

      const user = await UserService.update(req.params.id, req.body);
      res.data = user;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
