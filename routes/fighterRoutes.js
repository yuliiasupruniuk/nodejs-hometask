const { Router } = require("express");
const FighterService = require("../services/fighterService");
const { responseMiddleware } = require("../middlewares/response.middleware");
const {
  createFighterValid,
  updateFighterValid,
} = require("../middlewares/fighter.validation.middleware");

const router = Router();

// TODO: Implement route controllers for fighter

router.get(
  "/",
  async (req, res, next) => {
    try {
      const fighters = await FighterService.fetchAll();
      res.data = fighters;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.get(
  "/:id",
  async function (req, res, next) {
    try {
      const fighter = await FighterService.search({ id: req.params.id });
      if (!fighter) {
        res.status(404);
        throw new Error("Fighter not found!");
      }
      res.data = fighter;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.post(
  "/",
  createFighterValid,
  async (req, res, next) => {
    try {
      const check = await FighterService.checkExistance({
        name: req.body.name,
      });

      if (check) {
        res.status(400);
        throw new Error("Fighter already exists!");
      }

      if (!req.body.health) {
        req.body.health = 100;
      }
      const fighter = await FighterService.create(req.body);
      res.data = fighter;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.delete(
  "/:id",
  async (req, res, next) => {
    try {
      if (!FighterService.checkExistance({ id: req.params.id })) {
        res.status(404);
        throw new Error("Fighter doesn`t exist!");
      }
      const fighter = await FighterService.delete(req.params.id);
      res.data = fighter;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.put(
  "/:id",
  updateFighterValid,
  async (req, res, next) => {
    try {
      if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400);
        throw new Error("Update body must be not null!");
      }
      if (!FighterService.checkExistance({ id: req.params.id })) {
        res.status(404);
        throw new Error("Fighter doesn`t exist!");
      }

      const checkExistance = await FighterService.checkExistance({
        name: req.body.name,
      });
      if (checkExistance) {
        res.status(400);
        throw new Error("Fighter with such name already exists!");
      }

      const fighter = await FighterService.update(req.params.id, req.body);
      res.data = fighter;
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
