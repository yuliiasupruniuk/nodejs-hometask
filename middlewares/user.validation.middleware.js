const { user } = require("../models/user");
const ValidationService = require("../services/validationService");

const createUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during creation
  try {
    if (!req.body) {
      throw new Error("Request body must be not null!");
    }
    if (Object.keys(user).length - 1 !== Object.keys(req.body).length) {
      throw new Error("Number of fields isn't valid!");
    }

    const { firstName, lastName, email, phoneNumber, password } = req.body;

    const firstNameMsg = ValidationService.checkFirstName(firstName);
    if (firstNameMsg) {
      throw new Error(firstNameMsg);
    }

    const lastnameMsg = ValidationService.checkLastName(lastName);
    if (lastnameMsg) {
      throw new Error(lastnameMsg);
    }

    const emailCheckMsg = ValidationService.checkEmail(email);
    if (emailCheckMsg) {
      throw new Error(emailCheckMsg);
    }

    const phoneCheckMsg = ValidationService.checkPhone(phoneNumber);
    if (phoneCheckMsg) {
      throw new Error(phoneCheckMsg);
    }

    const passwordCheckMsg = ValidationService.checkPassword(password);
    if (passwordCheckMsg) {
      throw new Error(passwordCheckMsg);
    }

    next();
  } catch (error) {
    res.status(400).json({ error: true, message: error.message });
  }
};

const updateUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during update
  try {
    if (!req.body) {
      throw new Error("Request body must be not null!");
    }

    if (req.body.id) {
      throw new Error("Request body must not have id field!");
    }

    for (let key in req.body) {
      if (!user.hasOwnProperty(key)) {
        throw new Error(`Update data isn't valid!`);
      }
    }
    
    const { firstName, lastName, email, phoneNumber, password } = req.body;

    if (firstName) {
      const firstNameMsg = ValidationService.checkFirstName(firstName);
      if (firstNameMsg) {
        throw new Error(firstNameMsg);
      }
    }

    if (lastName) {
      const lastNameMsg = ValidationService.checkLastName(lastName);
      if (lastNameMsg) {
        throw new Error(lastNameMsg);
      }
    }

    if (email) {
      const emailCheckMsg = ValidationService.checkEmail(email);
      if (emailCheckMsg) {
        throw new Error(emailCheckMsg);
      }
    }

    if (phoneNumber) {
      const phoneCheckMsg = ValidationService.checkPhone(phoneNumber);
      if (phoneCheckMsg) {
        throw new Error(phoneCheckMsg);
      }
    }

    if (password) {
      const passwordCheckMsg = ValidationService.checkPassword(password);
      if (passwordCheckMsg) {
        throw new Error(passwordCheckMsg);
      }
    }

    next();
  } catch (err) {
    res.status(400).json({ error: true, message: err.message });
  }
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
