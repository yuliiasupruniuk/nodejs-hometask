const { fighter } = require("../models/fighter");
const ValidationService = require("../services/validationService");

const createFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during creation
  try {
    if (!req.body) {
      throw new Error("Request body must be not null!");
    }

    for (let key in req.body) {
      if (!fighter.hasOwnProperty(key)) {
        throw new Error(`Creation data isn't valid!`);
      }
    }

    if (
      Object.keys(fighter).length - 1 !== Object.keys(req.body).length &&
      Object.keys(fighter).length - 2 !== Object.keys(req.body).length
    ) {
      throw new Error("Number of fields isn't valid!");
    }

    if (req.body.id) {
      throw new Error("Request body must not have id field!");
    }

    const { name, health, power, defense } = req.body;

    const powerMsg = ValidationService.checkPower(power);
    if (powerMsg) {
      throw new Error(powerMsg);
    }

    const nameMsg = ValidationService.checkFirstName(name);
    if (nameMsg) {
      throw new Error(nameMsg);
    }

    const defenseMsg = ValidationService.checkDefense(defense);
    if (defenseMsg) {
      throw new Error(defenseMsg);
    }

    if (health) {
      const healthMsg = ValidationService.checkHealth(health);
      if (healthMsg) {
        throw new Error(healthMsg);
      }
    }

    next();
  } catch (error) {
    res.status(400).json({ error: true, message: error.message });
  }
};

const updateFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during update
  try {
    if (!req.body) {
      throw new Error("Request body must be not null!");
    }
    if (req.body.id) {
      throw new Error("Request body must not have id field!");
    }

    for (let key in req.body) {
      if (!fighter.hasOwnProperty(key)) {
        throw new Error(`Update data isn't valid!`);
      }
    }

    const { name, health, power, defense } = req.body;

    if (power) {
      const powerMsg = ValidationService.checkPower(power);
      if (powerMsg) {
        throw new Error(powerMsg);
      }
    }

    if (name) {
      const nameMsg = ValidationService.checkFirstName(name);
      if (nameMsg) {
        throw new Error(nameMsg);
      }
    }

    if (defense) {
      const defenseMsg = ValidationService.checkDefense(defense);
      if (defenseMsg) {
        throw new Error(defenseMsg);
      }
    }

    if (health) {
      const healthMsg = ValidationService.checkHealth(health);
      if (healthMsg) {
        throw new Error(healthMsg);
      }
    }
    next();
  } catch (err) {
    res.status(400).json({ error: true, message: err.message });
  }
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
