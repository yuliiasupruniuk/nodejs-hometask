class ValidationService {
  checkFirstName(name) {
    if (!name) {
      return "Firstname field cannot be empty!";
    }
    return false;
  }

  checkLastName(name) {
    if (!name) {
      return "Lastname field cannot be empty!";
    }
    return false;
  }

  checkEmail(email) {
    let checkEmail = email.split("@")[1];
    if (!email) {
      return "Email field cannot be empty!";
    }
    if (checkEmail !== "gmail.com") {
      return "Email must be gmail!";
    }
    return false;
  }

  checkPhone(phone) {
    if (!phone) {
      return "Phone field cannot be empty!";
    }
    if (phone.substr(0, 3) !== "+38" && phone.length !== 13) {
      return "Phone must be  +380xxxxxxxxx!";
    }
    return false;
  }

  checkPassword(password) {
    if (password) {
      if (password.length < 3) {
        return "Password length must be minimum 3 symbols!";
      }
    } else {
      return "Password field cannot be empty!";
    }

    return false;
  }

  checkPower(power) {
    if (!power) {
      return "Power field cannot be empty!";
    } else if (power > 1 && power < 100) {
      return false;
    }

    return "Power must be between 1 and 100";
  }

  checkDefense(defense) {
    if (!defense) {
      return "Defense field cannot be empty!";
    } else if (defense > 1 && defense < 10) {
      return false;
    }

    return "Defense must be between 1 and 10";
  }

  checkHealth(health) {
    if (health > 80 && defense < 120) {
      return false;
    }

    return "Health must be between 80 and 120";
  }
}

module.exports = new ValidationService();
