const { UserRepository } = require("../repositories/userRepository");

class UserService {
  // TODO: Implement methods to work with user

  fetchAll() {
    const users = UserRepository.getAll();
    if (!users) {
      return null;
    }
    return users;
  }

  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  create(userObj) {
    const user = UserRepository.create(userObj);
    if (!user) {
      return null;
    }
    return user;
  }

  checkExistance(props) {
    for (let key in props) {
      if (this.search({ [key]: props[key] })) {
        return true;
      }
    }
    return false;
  }

  delete(id) {
    const item = UserRepository.delete(id);
    if (!item) {
      return null;
    }
    return item;
  }

  update(id, body) {
    const item = UserRepository.update(id, body);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new UserService();
