const { FighterRepository } = require("../repositories/fighterRepository");

class FighterService {
  // TODO: Implement methods to work with fighters
  fetchAll() {
    const fighters = FighterRepository.getAll();
    if (!fighters) {
      return null;
    }
    return fighters;
  }

  search(search) {
    const item = FighterRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  create(userObj) {
    const user = FighterRepository.create(userObj);
    if (!user) {
      return null;
    }
    return user;
  }

  checkExistance(props) {
    for (let key in props) {
      if (this.search({ [key]: props[key] })) {
        return true;
      }
    }
    return false;
  }

  delete(id) {
    const item = FighterRepository.delete(id);
    if (!item) {
      return null;
    }
    return item;
  }

  update(id, body) {
    const item = FighterRepository.update(id, body);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new FighterService();
